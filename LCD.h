/* 
 * File:   LCD.h
 * Author: eluinstra
 *
 * Created on March 11, 2016, 11:52 PM
 * 
 * Copyright 2016 - Under creative commons license 3.0:
 *        Attribution-ShareAlike CC BY-SA
 * 
 * Ported from LiquidCrystal_I2C by F. Malpartida - fmalpartida@gmail.com
 * 
 */

#ifndef LCD_H
#define	LCD_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <xc.h>
#include "mcc_generated_files/mcc.h"
#include "LCD_I2C.h"

// LCD Commands
// ---------------------------------------------------------------------------
#define LCD_CLEARDISPLAY        0x01
#define LCD_RETURNHOME          0x02
#define LCD_ENTRYMODESET        0x04
#define LCD_DISPLAYCONTROL      0x08
#define LCD_CURSORSHIFT         0x10
#define LCD_FUNCTIONSET         0x20
#define LCD_SETCGRAMADDR        0x40
#define LCD_SETDDRAMADDR        0x80

// flags for display entry mode
// ---------------------------------------------------------------------------
#define LCD_ENTRYRIGHT          0x00
#define LCD_ENTRYLEFT           0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off and cursor control
// ---------------------------------------------------------------------------
#define LCD_DISPLAYON           0x04
#define LCD_DISPLAYOFF          0x00
#define LCD_CURSORON            0x02
#define LCD_CURSOROFF           0x00
#define LCD_BLINKON             0x01
#define LCD_BLINKOFF            0x00

// flags for display/cursor shift
// ---------------------------------------------------------------------------
#define LCD_DISPLAYMOVE         0x08
#define LCD_CURSORMOVE          0x00
#define LCD_MOVERIGHT           0x04
#define LCD_MOVELEFT            0x00

// flags for function set
// ---------------------------------------------------------------------------
#define LCD_8BITMODE            0x10
#define LCD_4BITMODE            0x00
#define LCD_2LINE               0x08
#define LCD_1LINE               0x00
#define LCD_5x10DOTS            0x04
#define LCD_5x8DOTS             0x00


// Define COMMAND and DATA LCD Rs (used by send method).
// ---------------------------------------------------------------------------
#define COMMAND                 0
#define DATA                    1
#define FOUR_BITS               2

#define HOME_CLEAR_EXEC      2000
#define BACKLIGHT_OFF           0
#define BACKLIGHT_ON          255

typedef enum
{
	POSITIVE,
	NEGATIVE
} backlight_polarity_t;
/*
typedef enum
{
	LCD_8BITMODE = 0x10,
	LCD_4BITMODE = 0x00,
	LCD_2LINE = 0x08,
	LCD_1LINE = 0x00,
	LCD_5x10DOTS = 0x04,
	LCD_5x8DOTS = 0x00
} display_function_t;
*/
typedef struct
{
	uint8_t function;
	uint8_t control;
	uint8_t mode;
	uint8_t numlines;
	uint8_t cols;
	backlight_polarity_t polarity;
} lcd_t;

lcd_t lcd;

void lcd_begin(uint8_t cols, uint8_t lines, uint8_t dotsize);
void clear();
void home();
void setCursor(uint8_t col, uint8_t row);
void noDisplay();
void display();
void noCursor();
void cursor();
void noBlink();
void blink();
void scrollDisplayLeft(void);
void scrollDisplayRight(void);
void leftToRight(void);
void rightToLeft(void);
void moveCursorRight(void);
void moveCursorLeft(void);
void autoscroll(void);
void noAutoscroll(void);
void backlight(void);
void noBacklight(void);
void on(void);
void off(void);
void write(uint8_t value);
void print(const uint8_t *buffer, uint8_t len);
void createChar(uint8_t location, uint8_t charmap[]);

#ifdef	__cplusplus
}
#endif

#endif	/* LCD_H */

