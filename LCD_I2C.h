/* 
 * File:   LCD_I2C.h
 * Author: eluinstra
 *
 * Created on March 12, 2016, 12:50 PM
 * 
 * Copyright 2016 - Under creative commons license 3.0:
 *        Attribution-ShareAlike CC BY-SA
 * 
 * Ported from LiquidCrystal_I2C by F. Malpartida - fmalpartida@gmail.com
 * 
 */

#ifndef LCD_I2C_H
#define	LCD_I2C_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include "mcc_generated_files/i2c.h"
#include "LCD.h"

#define LCD_NOBACKLIGHT 0x00
#define LCD_BACKLIGHT   0xFF
//PCF8574T
#define I2C_ADDR (0x27)
#define BACKLIGHT_PIN 3
#define EN_PIN 2
#define RW_PIN 1
#define RS_PIN 0
#define D4_PIN 4
#define D5_PIN 5
#define D6_PIN 6
#define D7_PIN 7

typedef struct
{
	uint8_t address;
	uint8_t backlightPinMask;
	uint8_t backlightStsMask;
	uint8_t en;
	uint8_t rw;
	uint8_t rs;
	uint8_t data_pins[4];
} lcd_i2c_t;

lcd_i2c_t lcd_i2c;

void lcd_i2c_init(void);
void lcd_i2c_begin(uint8_t cols, uint8_t lines, uint8_t dotsize);
void setBacklight(uint8_t value);
void send(uint8_t value, uint8_t mode);

#ifdef	__cplusplus
}
#endif

#endif	/* LCD_I2C_H */

