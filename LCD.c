/* 
 * File:   LCD.c
 * Author: eluinstra
 *
 * Created on March 11, 2016, 11:52 PM
 * 
 * Copyright 2016 - Under creative commons license 3.0:
 *        Attribution-ShareAlike CC BY-SA
 * 
 * Ported from LiquidCrystal_I2C by F. Malpartida - fmalpartida@gmail.com
 * 
 */
#include "LCD.h"

void command(uint8_t value) 
{
	send(value,COMMAND);
}

void clear()
{
	command(LCD_CLEARDISPLAY);
	__delay_us(HOME_CLEAR_EXEC);
}

void home()
{
	command(LCD_RETURNHOME);
	__delay_us(HOME_CLEAR_EXEC);
}

void setCursor(uint8_t col, uint8_t row)
{
	const uint8_t row_offsetsDef[]   = { 0x00, 0x40, 0x14, 0x54 }; // For regular LCDs
  const uint8_t row_offsetsLarge[] = { 0x00, 0x40, 0x10, 0x50 }; // For 16x4 LCDs
	if ( row >= lcd.numlines ) 
		row = lcd.numlines-1;    // rows start at 0
	// 16x4 LCDs have special memory map layout
	if ( lcd.cols == 16 && lcd.numlines == 4 )
		command(LCD_SETDDRAMADDR | (col + row_offsetsLarge[row]));
	else 
		command(LCD_SETDDRAMADDR | (col + row_offsetsDef[row]));
}

void noDisplay()
{
	lcd.control &= ~LCD_DISPLAYON;
	command(LCD_DISPLAYCONTROL | lcd.control);
}

void display()
{
	lcd.control |= LCD_DISPLAYON;
	command(LCD_DISPLAYCONTROL | lcd.control);
}

void noCursor()
{
	lcd.control &= ~LCD_CURSORON;
	command(LCD_DISPLAYCONTROL | lcd.control);
}

void cursor()
{
	lcd.control |= LCD_CURSORON;
	command(LCD_DISPLAYCONTROL | lcd.control);
}

void noBlink()
{
	lcd.control &= ~LCD_BLINKON;
	command(LCD_DISPLAYCONTROL | lcd.control);
}

void blink()
{
	lcd.control |= LCD_BLINKON;
	command(LCD_DISPLAYCONTROL | lcd.control);
}

void scrollDisplayLeft(void)
{
	command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}

void scrollDisplayRight(void)
{
	command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

void leftToRight(void)
{
	lcd.mode |= LCD_ENTRYLEFT;
	command(LCD_ENTRYMODESET | lcd.mode);
}

void rightToLeft(void)
{
	lcd.mode &= ~LCD_ENTRYLEFT;
	command(LCD_ENTRYMODESET | lcd.mode);
}

void moveCursorRight(void)
{
	command(LCD_CURSORSHIFT | LCD_CURSORMOVE | LCD_MOVERIGHT);
}

void moveCursorLeft(void)
{
	command(LCD_CURSORSHIFT | LCD_CURSORMOVE | LCD_MOVELEFT);
}

void autoscroll(void)
{
	lcd.mode |= LCD_ENTRYSHIFTINCREMENT;
	command(LCD_ENTRYMODESET | lcd.mode);
}

void noAutoscroll(void)
{
	lcd.mode &= ~LCD_ENTRYSHIFTINCREMENT;
	command(LCD_ENTRYMODESET | lcd.mode);
}

void backlight(void)
{
	setBacklight(255);
}

void noBacklight(void)
{
	setBacklight(0);
}

void on(void)
{
	display();
	backlight();
}

void off(void)
{
	noBacklight();
	noDisplay();
}

void write(uint8_t value)
{
	send(value,DATA);
}

void print(const uint8_t *buffer, uint8_t len)
{
  while (len--)
    write(*buffer++);
}

void createChar(uint8_t location, uint8_t charmap[])
{
	location &= 0x7;            // we only have 8 locations 0-7

	command(LCD_SETCGRAMADDR | (location << 3));
	__delay_us(30);

	for (uint8_t i = 0; i < 8; i++)
	{
		write(charmap[i]);      // call the virtual write method
		__delay_us(40);
	}
}

void lcd_begin(uint8_t cols, uint8_t lines, uint8_t dotsize)
{
  if (lines > 1) 
		lcd.function |= LCD_2LINE;
	lcd.numlines = lines;
	lcd.cols = cols;

	if ((dotsize != LCD_5x8DOTS) && (lines == 1)) 
		lcd.function |= LCD_5x10DOTS;

	// SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
	// according to datasheet, we need at least 40ms after power rises above 2.7V
	// before sending commands. Arduino can turn on way before 4.5V so we'll wait 
	// 50
	// ---------------------------------------------------------------------------
	__delay_ms(100); // 100ms delay

	if (!(lcd.function & LCD_8BITMODE)) 
	{
		// this is according to the hitachi HD44780 datasheet
		// figure 24, pg 46

		// we start in 8bit mode, try to set 4 bit mode
		// Special case of "Function Set"
		send(0x03,FOUR_BITS);
		__delay_us(4500); // wait min 4.1ms

		// second try
		send (0x03,FOUR_BITS);
		__delay_us(150); // wait min 100us

		// third go!
		send(0x03,FOUR_BITS);
		__delay_us(150); // wait min of 100us

		// finally, set to 4-bit interface
		send (0x02,FOUR_BITS);
		__delay_us(150); // wait min of 100us
	} 
	else 
	{
		// this is according to the hitachi HD44780 datasheet
		// page 45 figure 23

		// Send function set command sequence
		command(LCD_FUNCTIONSET | lcd.function);
		__delay_ms(5);  // wait more than 4.1ms

		// second try
		command(LCD_FUNCTIONSET | lcd.function);
		__delay_us(150);

		// third go
		command(LCD_FUNCTIONSET | lcd.function);
		__delay_us(150);
	}
	command(LCD_FUNCTIONSET | lcd.function);
	__delay_us(60);

	lcd.control = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;  
	display();

	clear();

	lcd.mode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
	command(LCD_ENTRYMODESET | lcd.mode);

	backlight();
}