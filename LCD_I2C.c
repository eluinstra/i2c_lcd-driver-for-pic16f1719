/* 
 * File:   LCD_I2C.c
 * Author: eluinstra
 *
 * Created on March 12, 2016, 12:50 PM
 * 
 * Copyright 2016 - Under creative commons license 3.0:
 *        Attribution-ShareAlike CC BY-SA
 * 
 * Ported from LiquidCrystal_I2C by F. Malpartida - fmalpartida@gmail.com
 * 
 */
# include "LCD_I2C.h"

void lcd_i2c_init(void)
{
	lcd_i2c.address = I2C_ADDR;
	lcd_i2c.backlightPinMask = (1 << BACKLIGHT_PIN);
	lcd_i2c.backlightStsMask = lcd_i2c.backlightPinMask & LCD_NOBACKLIGHT;
	lcd_i2c.en = (1 << EN_PIN);
	lcd_i2c.rw = (1 << RW_PIN);
	lcd_i2c.rs = (1 << RS_PIN);
	lcd_i2c.data_pins[0] = (1 << D4_PIN);
	lcd_i2c.data_pins[1] = (1 << D5_PIN);
	lcd_i2c.data_pins[2] = (1 << D6_PIN);
	lcd_i2c.data_pins[3] = (1 << D7_PIN);
	lcd.polarity = POSITIVE;
}

void lcd_i2c_begin(uint8_t cols, uint8_t lines, uint8_t dotsize)
{
	lcd.function = LCD_4BITMODE | LCD_1LINE | LCD_5x8DOTS;
	I2C_MESSAGE_STATUS status = I2C_MESSAGE_PENDING;
	uint8_t buffer[] = {0x00};
	I2C_MasterWrite(buffer,sizeof(buffer),lcd_i2c.address,&status);
	while (status == I2C_MESSAGE_PENDING);
	lcd_begin(cols,lines,dotsize);
}

void setBacklight(uint8_t value)
{
	I2C_MESSAGE_STATUS status = I2C_MESSAGE_PENDING;
	// Check if backlight is available
	if (lcd_i2c.backlightPinMask != 0x0)
	{
		// Check for polarity to configure mask accordingly
		if  (((lcd.polarity == POSITIVE) && (value > 0)) || ((lcd.polarity == NEGATIVE ) && ( value == 0 )))
		  lcd_i2c.backlightStsMask = lcd_i2c.backlightPinMask & LCD_BACKLIGHT;
		else 
			lcd_i2c.backlightStsMask = lcd_i2c.backlightPinMask & LCD_NOBACKLIGHT;
		I2C_MasterWrite(&lcd_i2c.backlightStsMask,1,lcd_i2c.address,&status);
		while (status == I2C_MESSAGE_PENDING);
	}
}

void pulseEnable(uint8_t data)
{
	{
		I2C_MESSAGE_STATUS status = I2C_MESSAGE_PENDING;
		uint8_t _data = (data | lcd_i2c.en);
		I2C_MasterWrite(&_data,1,lcd_i2c.address,&status);
		while (status == I2C_MESSAGE_PENDING);
	}
	{
		I2C_MESSAGE_STATUS status = I2C_MESSAGE_PENDING;
		uint8_t _data = (data & ~lcd_i2c.en);
		I2C_MasterWrite(&_data,1,lcd_i2c.address,&status);
		while (status == I2C_MESSAGE_PENDING);
	}
}

void write4bits(uint8_t value, uint8_t mode) 
{
	uint8_t pinMapValue = 0;
	for (uint8_t i = 0; i < 4; i++)
	{
		if ((value & 0x1) == 1)
			pinMapValue |= lcd_i2c.data_pins[i];
		value = (value >> 1);
	}
	if (mode == DATA)
		mode = lcd_i2c.rs;
	pinMapValue |= mode | lcd_i2c.backlightStsMask;
	pulseEnable(pinMapValue);
}

void send(uint8_t value, uint8_t mode) 
{
	if (mode == FOUR_BITS)
		write4bits((value & 0x0F),COMMAND);
	else 
	{
		write4bits((value >> 4),mode);
		write4bits((value & 0x0F),mode);
	}
}
